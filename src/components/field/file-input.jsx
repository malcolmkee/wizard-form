import React from 'react';
import { callAll, noop } from '../../util/fn-util';
import { getClassName } from '../../util/string-util';
import { Button } from '../button';
import './file-input.scss';

export class FileInput extends React.Component {
  render() {
    const { onChangeValue, className, id, onChange, disabled, label, ...props } = this.props;
    return (
      <div className="file-input">
        <input
          type="file"
          className="file-input--input"
          id={id}
          onChange={callAll(onChange, this.handleChange)}
          value=""
          {...props}
        />
        <Button
          as="label"
          className={getClassName('file-input--button', className, disabled && 'disabled')}
          htmlFor={id}
          tabIndex={-1} // For accesibility, tab should not focus on label, it should focus on the input instead
          bordered
          rounded
        >
          {label}
        </Button>
      </div>
    );
  }

  handleChange = e => !this.props.disabled && this.props.onChangeValue(Array.from(e.target.files));

  static defaultProps = {
    id: 'file-input',
    onChangeValue: noop,
    label: 'Upload'
  };
}
