import React from 'react';
import { callAll, noop } from '../../util/fn-util';
import { getClassName } from '../../util/string-util';
import './text-input.scss';

export class TextInput extends React.Component {
  render() {
    const { className, onChange, onChangeValue, ...props } = this.props;

    return (
      <textarea
        className={getClassName('text-input', className)}
        onChange={callAll(this.handleChange, onChange)}
        {...props}
      />
    );
  }

  handleChange = e => this.props.onChangeValue(e.target.value);

  static defaultProps = {
    onChangeValue: noop
  };
}
