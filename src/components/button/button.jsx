import React from 'react';
import { getClassName } from '../../util/string-util';
import './button.scss';

export const Button = ({
  primary,
  rounded,
  bordered,
  className, // custom className
  tabIndex = 0,
  as: Component = 'button',
  ...props
}) => (
  <Component
    className={getClassName(
      'button',
      className,
      primary && 'primary',
      rounded && 'rounded',
      bordered && 'bordered'
    )}
    tabIndex={tabIndex}
    {...props}
  />
);
