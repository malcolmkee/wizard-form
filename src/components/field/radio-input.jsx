import React from 'react';
import { callAll, noop } from '../../util/fn-util';
import { getClassName } from '../../util/string-util';
import { Button } from '../button';

export class RadioInput extends React.Component {
  state = {
    selected: this.props.value
  };

  render() {
    return React.Children.map(this.props.children, child =>
      React.cloneElement(child, {
        selected: this.state.selected,
        select: this.handleSelect
      })
    );
  }

  handleSelect = value =>
    this.setState({ selected: value }, () => this.props.onChangeValue(this.state.selected));
}

export const RadioOption = ({
  selected,
  select = noop,
  value,
  display,
  onClick,
  className,
  ...props
}) => (
  <Button
    className={getClassName('radio-option', className)}
    onClick={callAll(onClick, () => select(value))}
    primary={selected === value}
    bordered={true}
    {...props}
  >
    {display}
  </Button>
);
