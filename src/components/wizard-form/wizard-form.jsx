import React from 'react';
import { ProgressBar } from '../progress-bar';
import { Wizard, Step, withWizard } from '../wizard';
import { WizardFormFooter } from './wizard-form-footer';
import { WizardFormStep, validateStep } from './wizard-form-step';
import { WizardFormSummary } from './wizard-form-summary';
import './wizard-form.scss';

/**
 * props {
 *      title: string
 *      questions: {
 *          id: number
 *          question_type: 'TextQuestion' | 'ImageQuestion' | 'CheckboxQuestion' | 'RadioQuestion'
 *          prompt: string
 *          is_required: boolean
 *          min_char_length?: number
 *          options?: {
 *            value: any
 *            display: string
 *          }[]
 *      }[]
 *      onSubmit: (answers: any[]) => void
 * }
 */
export class WizardForm extends React.Component {
  state = {
    values: {}
  };

  render() {
    const props = this.props;
    return (
      <div className="wizard-form">
        <Wizard>
          <header className="wizard-form--header">
            <h1>{props.title}</h1>
            <WizardFormProgress totalStep={props.questions.length} />
          </header>
          <main>
            {props.questions.map((question, i) => (
              <WizardFormStep
                question={question}
                stepNumber={i}
                value={this.state.values[question.id]}
                onChangeValue={this.handleSetValue(question.id)}
                stepIsValid={this.stepIsValid(question)}
                key={question.id}
              />
            ))}
            <Step stepNumber={props.questions.length}>
              <WizardFormSummary questions={props.questions} answers={this.state.values} />
              <WizardFormFooter showSubmit onSubmit={this.handleSubmit} />
            </Step>
          </main>
        </Wizard>
      </div>
    );
  }

  stepIsValid = question => validateStep(question, this.state.values[question.id]);

  handleSetValue = id => value =>
    this.setState(prevState => ({ values: { ...prevState.values, [id]: value } }));

  handleSubmit = () => this.props.onSubmit(this.state.values);
}

const WizardFormProgressContainer = ({ currentStep, totalStep }) => (
  <ProgressBar percentage={(currentStep / totalStep) * 100} />
);

const WizardFormProgress = withWizard(WizardFormProgressContainer);
