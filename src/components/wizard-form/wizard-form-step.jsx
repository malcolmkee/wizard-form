import React from 'react';
import { is } from '../../util/is';
import { getClassName } from '../../util/string-util';
import { Step } from '../wizard';
import { Button } from '../button';
import {
  FileInput,
  TextInput,
  CheckboxInput,
  CheckboxOption,
  RadioInput,
  RadioOption
} from '../field';
import { Image } from '../image';
import { WizardFormFooter } from './wizard-form-footer';

export const WizardFormStep = ({ question, stepNumber, value, onChangeValue, stepIsValid }) => {
  switch (question.question_type) {
    case 'ImageQuestion':
      return (
        <Step stepNumber={stepNumber}>
          <ImageQuestion
            id={question.id}
            prompt={question.prompt}
            required={question.is_required}
            value={value}
            onChangeValue={onChangeValue}
          />
          <WizardFormFooter hidePrev={stepNumber === 0} allowNext={stepIsValid} />
        </Step>
      );

    case 'TextQuestion':
      return (
        <Step stepNumber={stepNumber}>
          <TextQuestion
            id={question.id}
            prompt={question.prompt}
            required={question.is_required}
            value={value}
            onChangeValue={onChangeValue}
          />
          <WizardFormFooter hidePrev={stepNumber === 0} allowNext={stepIsValid} />
        </Step>
      );

    case 'CheckboxQuestion':
      return (
        <Step stepNumber={stepNumber}>
          <CheckboxQuestion
            id={question.id}
            prompt={question.prompt}
            required={question.is_required}
            options={question.options}
            value={value}
            onChangeValue={onChangeValue}
          />
          <WizardFormFooter hidePrev={stepNumber === 0} allowNext={stepIsValid} />
        </Step>
      );

    case 'RadioQuestion':
      return (
        <Step stepNumber={stepNumber}>
          <RadioQuestion
            id={question.id}
            prompt={question.prompt}
            required={question.is_required}
            options={question.options}
            value={value}
            onChangeValue={onChangeValue}
          />
          <WizardFormFooter hidePrev={stepNumber === 0} allowNext={stepIsValid} />
        </Step>
      );

    default:
      return null;
  }
};

export const validateStep = (question, value) =>
  !question.is_required ||
  (question.question_type === 'TextQuestion' &&
    is.FilledText(value) &&
    value.length >= question.min_char_length) ||
  (question.question_type === 'ImageQuestion' && is.FilledArray(value)) ||
  (question.question_type === 'CheckboxQuestion' && is.FilledArray(value)) ||
  (question.question_type === 'RadioQuestion' && !is.Nil(value));

const TextQuestion = ({ id, prompt, required, value = '', onChangeValue }) => (
  <div className="wizard-form--question">
    <QuestionLabel id={id} prompt={prompt} required={required} />
    <div>
      <TextInput value={value} onChangeValue={onChangeValue} rows={5} id={getId(id)} autoFocus />
    </div>
  </div>
);

const ImageQuestion = ({ id, prompt, required, value, onChangeValue }) => (
  <div className="wizard-form--question">
    <QuestionLabel id={id} prompt={prompt} required={required} />
    <div>
      {value &&
        value.map((file, i) => (
          <Image className="wizard-form--question-image" file={file} key={i} />
        ))}
    </div>
    <div>
      {value ? (
        <Button onClick={() => onChangeValue(undefined)} rounded bordered>
          Remove
        </Button>
      ) : (
        <FileInput onChangeValue={onChangeValue} id={getId(id)} accept="image/*" multiple />
      )}
    </div>
  </div>
);

const CheckboxQuestion = ({ id, prompt, required, options, value, onChangeValue }) => (
  <div className="wizard-form--question">
    <QuestionLabel id={id} prompt={prompt} required={required} />
    <div>
      <CheckboxInput value={value} onChangeValue={onChangeValue}>
        {options.map((option, i) => (
          <CheckboxOption
            className="wizard-form--question-option"
            value={option.value}
            display={option.display}
            key={i}
          />
        ))}
      </CheckboxInput>
    </div>
  </div>
);

const RadioQuestion = ({ id, prompt, required, options, value, onChangeValue }) => (
  <div className="wizard-form--question">
    <QuestionLabel id={id} prompt={prompt} required={required} />
    <div>
      <RadioInput value={value} onChangeValue={onChangeValue}>
        {options.map((option, i) => (
          <RadioOption
            className="wizard-form--question-option"
            value={option.value}
            display={option.display}
            key={i}
          />
        ))}
      </RadioInput>
    </div>
  </div>
);

const QuestionLabel = ({ id, prompt, required }) => (
  <React.Fragment>
    <label className="wizard-form--question-label" htmlFor={getId(id)}>
      {prompt}
    </label>
    <div className={getClassName('wizard-form--question-required', !required && 'invisible')}>
      *Required
    </div>
  </React.Fragment>
);

const getId = questionId => `question-${questionId}`;
