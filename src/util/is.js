const isNil = value => typeof value === 'undefined' || value === null;
const isFilledArray = value => Array.isArray(value) && value.length > 0;
const isFilledText = value => typeof value === 'string' && value.length > 0;
const isFunction = value => typeof value === 'function';

export const is = {
  FilledArray: isFilledArray,
  FilledText: isFilledText,
  Function: isFunction,
  Nil: isNil
};
