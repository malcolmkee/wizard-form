import { is } from './is';

/**
 * Noop for default value
 */
export const noop = () => {};

/**
 * Utility to compose the functions so you can call them together
 * @param fns functions that you want to call
 */
export const callAll = (...fns) => (...args) => fns.forEach(fn => is.Function(fn) && fn(...args));
