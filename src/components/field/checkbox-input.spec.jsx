import React from 'react';
import { render, fireEvent, cleanup } from 'react-testing-library';
import { CheckboxInput, CheckboxOption } from './checkbox-input';

afterEach(cleanup);

describe('<CheckboxInput />', () => {
  const Component = ({ onChangeValue }) => (
    <CheckboxInput onChangeValue={onChangeValue}>
      <CheckboxOption value={1} data-testid="option-1" display="One" />
      <CheckboxOption value={2} data-testid="option-2" display="Two" />
      <CheckboxOption value={3} data-testid="option-3" display="Three" />
      <CheckboxOption value={4} data-testid="option-4" display="Four" />
    </CheckboxInput>
  );

  test('click option will include it to result', () => {
    const onChangeValueHandler = jest.fn();
    const { getByTestId } = render(<Component onChangeValue={onChangeValueHandler} />);

    fireEvent.click(getByTestId('option-1'));
    expect(onChangeValueHandler).toBeCalledWith([1]);

    onChangeValueHandler.mockClear();

    fireEvent.click(getByTestId('option-3'));
    expect(onChangeValueHandler).toBeCalledWith([1, 3]);
  });

  test('click option that has been selected will unselect it', () => {
    const onChangeValueHandler = jest.fn();
    const { getByTestId } = render(<Component onChangeValue={onChangeValueHandler} />);

    fireEvent.click(getByTestId('option-1'));
    fireEvent.click(getByTestId('option-3'));

    onChangeValueHandler.mockClear();

    fireEvent.click(getByTestId('option-1'));
    expect(onChangeValueHandler).toBeCalledWith([3]);
  });
});
