import React from 'react';

/**
 * Image able to load image via url or file that is selected locally
 */
export class Image extends React.Component {
  state = {
    fileUrl: null
  };

  render() {
    const { file, alt = 'placeholder', src, ...props } = this.props;

    return <img alt={alt} src={src || this.state.fileUrl} {...props} />;
  }

  componentDidMount() {
    if (this.props.file) {
      const fileUrl = createUrl(this.props.file);
      this.setState({ fileUrl });
    }
  }

  componentWillUnmount() {
    revokeUrl(this.state.fileUrl);
  }
}

const URL = window.URL || window.webkitURL;

const createUrl = file => URL.createObjectURL(file);

const revokeUrl = fileUrl => URL.revokeObjectURL(fileUrl);
