import React from 'react';
import { render, fireEvent, cleanup } from 'react-testing-library';
import { RadioInput, RadioOption } from './radio-input';

afterEach(cleanup);

describe('<RadioInput />', () => {
  const Component = ({ onChangeValue }) => (
    <RadioInput onChangeValue={onChangeValue}>
      <RadioOption value={1} data-testid="option-1" display="One" />
      <RadioOption value={2} data-testid="option-2" display="Two" />
      <RadioOption value={3} data-testid="option-3" display="Three" />
      <RadioOption value={4} data-testid="option-4" display="Four" />
    </RadioInput>
  );

  test('click option will set it to value', () => {
    const onChangeValueHandler = jest.fn();
    const { getByTestId } = render(<Component onChangeValue={onChangeValueHandler} />);

    fireEvent.click(getByTestId('option-1'));
    expect(onChangeValueHandler).toBeCalledWith(1);

    onChangeValueHandler.mockClear();

    fireEvent.click(getByTestId('option-3'));
    expect(onChangeValueHandler).toBeCalledWith(3);
  });
});
