import { is } from './is';

describe('is.Nil', () => {
  test('is.Nil(null) -> true', () => {
    expect(is.Nil(null)).toBe(true);
  });

  test('is.Nil(undefined) -> true', () => {
    expect(is.Nil(undefined)).toBe(true);
  });

  test('is.Nil("") -> false', () => {
    expect(is.Nil('')).toBe(false);
  });

  test('is.Nil({}) -> false', () => {
    expect(is.Nil({})).toBe(false);
  });

  test('is.Nil(date) -> false', () => {
    const date = new Date();
    expect(is.Nil(date)).toBe(false);
  });
});

describe('is.FilledText', () => {
  test('is.FilledText("234") -> true', () => {
    expect(is.FilledText('234')).toBe(true);
  });

  test('is.FilledText("ab") -> false', () => {
    expect(is.FilledText('ab')).toBe(true);
  });

  test('is.FilledText("") -> false', () => {
    expect(is.FilledText('')).toBe(false);
  });

  test('is.FilledText(234) -> false', () => {
    expect(is.FilledText(234)).toBe(false);
  });

  test('is.FilledText(null) -> false', () => {
    expect(is.FilledText(null)).toBe(false);
  });

  test('is.FilledText(undefined) -> false', () => {
    expect(is.FilledText(undefined)).toBe(false);
  });
});
