import React from 'react';
import { Button } from '../button';
import { PrevButton, NextButton } from '../wizard';

export const WizardFormFooter = ({ hidePrev, allowNext, showSubmit, onSubmit }) => (
  <footer className="wizard-form--footer">
    {!hidePrev && <PrevButton rounded>Prev</PrevButton>}
    <div className="wizard-form--footer-filler" />
    {showSubmit ? (
      <Button onClick={onSubmit} primary rounded>
        Submit
      </Button>
    ) : (
      <NextButton primary rounded disabled={!allowNext}>
        Next >
      </NextButton>
    )}
  </footer>
);
