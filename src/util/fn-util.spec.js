import { callAll } from './fn-util';

describe('callAll', () => {
  test('all functions will be invoked', () => {
    const fn1 = jest.fn();
    const fn2 = jest.fn();
    const result = callAll(fn1, fn2);

    result();

    expect(fn1).toHaveBeenCalledTimes(1);
    expect(fn2).toHaveBeenCalledTimes(1);
  });

  test('all functions will be invoked with arguments', () => {
    const fn1 = jest.fn();
    const fn2 = jest.fn();
    const args = [true, '2934'];
    const result = callAll(fn1, fn2);

    result(...args);

    expect(fn1).toBeCalledWith(...args);
    expect(fn2).toBeCalledWith(...args);
  });
});
