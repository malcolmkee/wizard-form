import React from 'react';
import { callAll, noop } from '../../util/fn-util';
import { getClassName } from '../../util/string-util';
import { Button } from '../button';

export class CheckboxInput extends React.Component {
  state = {
    selected: this.props.value
  };

  render() {
    return React.Children.map(this.props.children, child =>
      React.cloneElement(child, {
        selected: this.state.selected,
        toggleSelect: this.handleToggleSelect
      })
    );
  }

  handleToggleSelect = value =>
    this.setState(
      prevState => ({
        selected:
          prevState.selected.indexOf(value) === -1
            ? prevState.selected.concat(value)
            : prevState.selected.filter(item => item !== value)
      }),
      () => this.props.onChangeValue(this.state.selected)
    );

  static defaultProps = {
    value: []
  };
}

export const CheckboxOption = ({
  selected = [],
  toggleSelect = noop,
  value,
  display,
  onClick,
  className,
  ...props
}) => (
  <Button
    className={getClassName('checkbox-option', className)}
    onClick={callAll(onClick, () => toggleSelect(value))}
    primary={selected.indexOf(value) !== -1}
    bordered={true}
    {...props}
  >
    {display}
  </Button>
);
