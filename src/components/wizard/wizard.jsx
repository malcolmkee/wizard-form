import React from 'react';
import { callAll, noop } from '../../util/fn-util';
import { Button } from '../button';

const DEFAULT_CONTEXT = {
  currentStep: 1,
  onNext: noop,
  onPrev: noop
};

const WizardContext = React.createContext(DEFAULT_CONTEXT);

/**
 * Wizard is a component that manage the states of a step-by-step process
 * together with Step, NextButton, and PrevButton components
 * props: {
 *    startFrom?: number
 * }
 */
export class Wizard extends React.Component {
  handleNext = () =>
    this.setState(
      prevState => ({ currentStep: prevState.currentStep + 1 }),
      () => this.props.onChangeStep(this.state.currentStep)
    );

  handlePrev = () =>
    this.setState(
      prevState => ({ currentStep: prevState.currentStep - 1 }),
      () => this.props.onChangeStep(this.state.currentStep)
    );

  state = {
    currentStep: this.props.startFrom,
    onNext: this.handleNext,
    onPrev: this.handlePrev
  };

  render() {
    return (
      <WizardContext.Provider value={this.state}>{this.props.children}</WizardContext.Provider>
    );
  }

  static defaultProps = {
    startFrom: 0,
    onChangeStep: noop
  };
}

/**
 * Render children conditionally if stepNumber matches the Wizard currentStep
 */
export const Step = ({ stepNumber, children }) => (
  <WizardContext.Consumer>
    {({ currentStep }) => (currentStep === stepNumber ? children : null)}
  </WizardContext.Consumer>
);

/**
 * Increment step number of Wizard
 */
export const NextButton = ({ as: Component = Button, onClick, ...props }) => (
  <WizardContext.Consumer>
    {({ onNext }) => <Component onClick={callAll(onNext, onClick)} {...props} />}
  </WizardContext.Consumer>
);

/**
 * Decrement step number of Wizard
 */
export const PrevButton = ({ as: Component = Button, onClick, ...props }) => (
  <WizardContext.Consumer>
    {({ onPrev }) => <Component onClick={callAll(onPrev, onClick)} {...props} />}
  </WizardContext.Consumer>
);

/**
 * HOC to inject wizardProps
 * @param {React.ComponentType} Component
 */
export const withWizard = Component => props => (
  <WizardContext.Consumer>
    {wizardProps => <Component {...props} {...wizardProps} />}
  </WizardContext.Consumer>
);
