import React from 'react';
import { joinString } from '../../util/string-util';
import { Image } from '../image';

const joinOptions = joinString(', ');

export const WizardFormSummary = ({ questions, answers }) => (
  <div className="wizard-form--summary">
    {questions.map(question => (
      <div className="wizard-form--summary-item" key={question.id}>
        <div className="wizard-form--summary-question">{question.prompt}</div>
        <div className="wizard-form--summary-answer">
          {getAnswerDisplay(question, answers[question.id])}
        </div>
      </div>
    ))}
  </div>
);

const getAnswerDisplay = (question, answer) => {
  switch (question.question_type) {
    case 'ImageQuestion':
      return answer.map((file, i) => (
        <Image file={file} className="wizard-form--question-image" key={i} />
      ));

    case 'TextQuestion':
      return answer;

    case 'CheckboxQuestion':
      return joinOptions(
        ...answer
          .map(value => question.options.find(option => option.value === value))
          .map(option => option.display)
      );

    case 'RadioQuestion':
      return question.options
        .filter(option => option.value === answer)
        .map(option => option.display);

    default:
      return null;
  }
};
