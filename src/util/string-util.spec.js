import { joinString } from './string-util';

describe('joinString', () => {
  test('join texts with comma', () => {
    const joinText = joinString(', ');
    const result = joinText('Malcolm', 'Kee');
    expect(result).toBe('Malcolm, Kee');
  });

  test('empty string with be ignored', () => {
    const joinText = joinString(', ');
    const result = joinText('Malcolm', '', '', 'Kee', '');
    expect(result).toBe('Malcolm, Kee');
  });

  test('false condition with be ignored', () => {
    const condition = false;

    const joinText = joinString(', ');
    const result = joinText('Malcolm', condition && 'Who', condition && 'Nicky', 'Kee', '');
    expect(result).toBe('Malcolm, Kee');
  });
});
