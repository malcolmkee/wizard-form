import React from 'react';
import { WizardForm } from './components/wizard-form';
import questionData from './questionData.json';

const App = () => (
  <div className="App">
    <WizardForm
      title={questionData.title}
      questions={questionData.questions}
      onSubmit={answers => console.log({ answers })}
    />
  </div>
);

export default App;
