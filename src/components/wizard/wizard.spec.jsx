import React from 'react';
import { render, fireEvent, cleanup } from 'react-testing-library';
import { Wizard, Step, NextButton, PrevButton } from './wizard';

afterEach(cleanup);

describe('<Wizard />', () => {
  const Component = () => (
    <Wizard>
      <Step stepNumber={0}>
        Step <span data-testid="step-indicator">0</span>
        <NextButton data-testid="next-0">Next</NextButton>
      </Step>
      <Step stepNumber={1}>
        Step <span data-testid="step-indicator">1</span>
        <PrevButton data-testid="prev-1">Previous</PrevButton>
        <NextButton data-testid="next-1">Next</NextButton>
      </Step>
    </Wizard>
  );

  test('click next will navigate to next page', () => {
    const { getByTestId } = render(<Component />);

    expect(getByTestId('step-indicator').innerHTML).toBe('0');

    fireEvent.click(getByTestId('next-0'));

    expect(getByTestId('step-indicator').innerHTML).toBe('1');
  });

  test('click prev will navigate to prev page', () => {
    const { getByTestId } = render(<Component />);

    fireEvent.click(getByTestId('next-0'));

    expect(getByTestId('step-indicator').innerHTML).toBe('1');

    fireEvent.click(getByTestId('prev-1'));

    expect(getByTestId('step-indicator').innerHTML).toBe('0');
  });
});
