import React from 'react';
import './progress-bar.scss';

export const ProgressBar = ({ percentage }) => (
  <div className="progress-bar">
    <div className="progress-bar--line" style={{ width: `${percentage}%` }} />
    <div className="progress-bar--fullline" />
  </div>
);
